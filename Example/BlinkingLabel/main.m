//
//  main.m
//  BlinkingLabel
//
//  Created by Ben LIU on 03/22/2016.
//  Copyright (c) 2016 Ben LIU. All rights reserved.
//

@import UIKit;
#import "BLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BLAppDelegate class]));
    }
}
