//
//  BLAppDelegate.h
//  BlinkingLabel
//
//  Created by Ben LIU on 03/22/2016.
//  Copyright (c) 2016 Ben LIU. All rights reserved.
//

@import UIKit;

@interface BLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
